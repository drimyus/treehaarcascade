import cv2
import dlib
import threading
import time
import sys
import numpy as np
from imutils.object_detection import non_max_suppression
from datetime import datetime
import math
import argparse
import os


parser = argparse.ArgumentParser()
parser.add_argument("--input_path", help="path of input image or video files", default="")
parser.add_argument("--annotation", help="show the detected result with 'line' or 'rect'", default="rect")
parser.add_argument("--save", help="flag whether the result will be saved or not", default=True, type=bool)
arg = parser.parse_args()


HIST_STEPS = 32
g_my_cascade, g_distance, g_upsampling = None, None, None


def _getLabelFromColor(color):

    # dis_pos, dis_neg = 0.0, 0.0
    # for i in range(len(color)):
    #     dis_neg += (color[i] - negative_color[i]) ** 2
    #     dis_pos += (color[i] - positive_color[i]) ** 2
    # if dis_pos > dis_neg:
    #     return 'neg'
    # else:
    #     return 'pos'

    if color.index(max(color)) == 1:
        return 'neg'
    # elif color[0] > color[2]:
    #     return 'neg'
    else:
        return 'pos'


def _getColorFeature(sub):

    hist_blue = cv2.calcHist([sub], [0], None, [HIST_STEPS], [0, 256])
    hist_green = cv2.calcHist([sub], [1], None, [HIST_STEPS], [0, 256])
    hist_red = cv2.calcHist([sub], [2], None, [HIST_STEPS], [0, 256])

    blue = np.argmax(hist_blue) * 255 / HIST_STEPS + 0.5 * 255 / HIST_STEPS
    green = np.argmax(hist_green) * 255 / HIST_STEPS + 0.5 * 255 / HIST_STEPS
    red = np.argmax(hist_red) * 255 / HIST_STEPS + 0.5 * 255 / HIST_STEPS

    return [blue, green, red]


negative_color = [110, 119, 92]
positive_color = [76, 80, 85]


def _detectTrees(frame, height, width):

    global g_my_cascade, g_distance, g_upsampling

    # detect positive rect
    pos_rects = []
    for detector in g_my_cascade:
        rects = detector.detectMultiScale(frame, g_upsampling, g_distance)

        for (x, y, w, h) in rects:
            sub_img = frame[int(y + h / 3):int(y + h * 2 / 3), int(x + w / 3):int(x + w * 2 / 3)]
            # sub_img = frame[int(y):int(y + h), int(x):int(x + w)]
            color = _getColorFeature(sub_img)
            if _getLabelFromColor(color) == 'pos' and y > height / 3:
                pos_rects.append([x, y, x + w, y + h])

    # merge the detected rects
    boxs = np.array(pos_rects)
    pick = non_max_suppression(boxs, probs=None, overlapThresh=0.05)

    return pick


def _doRecognizePerson(tid):
    time.sleep(0.5)
    # sys.stdout.write(tid)


def videoDetectTree(video_path, save, skip=3, ann="rect"):

    if video_path == '0':
        cap = cv2.VideoCapture(int(video_path))
    else:
        cap = cv2.VideoCapture(video_path)

    # Setting the output video properties
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # setting for saving video files
    if save:
        out_path = './Result.mp4'
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(out_path,
                              fourcc,
                              25.0,
                              (width, height))

    rectangleColor = (255, 255, 0)
    rightPointColor = (0, 0, 255)
    leftPointColor = (0, 0, 255)
    frameCounter = 0
    treeTrackers = {}
    currentTreeID = 1

    old_l_a, old_l_b = 0.0, 0.0
    old_r_a, old_r_b = 0.0, 0.0
    try:

        while True:
            # Retrieve the latest image from the webcam
            rc, baseImage = cap.read()
            if not rc:
                print('Error: Camera was disconnect.')
                break

            # Check if key: 'q' -> break
            pressedKey = cv2.waitKey(2)
            if pressedKey == ord('q'):
                break

            resultImage = baseImage.copy()

            frameCounter += 1
            tidsToDelete = []

            for tid in treeTrackers.keys():
                trackingQuality = treeTrackers[tid].update(baseImage)

                # If the tracking quality is good enough, we must delete this tracker
                if trackingQuality < 7:
                    tidsToDelete.append(tid)

            for tid in tidsToDelete:
                # sys.stdout.write("Removing tid " + str(tid) + " from list of trackers \n")
                treeTrackers.pop(tid, None)
           
            if (frameCounter % skip) == 0:

                trees = _detectTrees(baseImage, height, width)

                for (_x, _y, _x2, _y2) in trees:
                    x = int(_x)
                    y = int(_y)
                    w = int(_x2 - x)
                    h = int(_y2 - y)

                    x_bar = x + 0.5 * w
                    y_bar = y + 0.5 * h
                    
                    matchedTid = None

                    for tid in treeTrackers.keys():

                        tracked_position = treeTrackers[tid].get_position()
                        t_x = int(tracked_position.left())
                        t_y = int(tracked_position.top())
                        t_w = int(tracked_position.width())
                        t_h = int(tracked_position.height())

                        t_x_bar = t_x + 0.5 * t_w
                        t_y_bar = t_y + 0.5 * t_h

                        if ((t_x <= x_bar <= (t_x + t_w)) and
                                (t_y <= y_bar <= (t_y + t_h)) and
                                (x <= t_x_bar <= (x + w)) and
                                (y <= t_y_bar <= (y + h))):
                            matchedTid = tid

                    # If no matched tid, then we have to create a new tracker
                    if matchedTid is None:
                        # sys.stdout.write("Creating new tracker " + str(currentTreeID) + "\n")

                        # Create and store the tracker
                        tracker = dlib.correlation_tracker()
                        tracker.start_track(baseImage, dlib.rectangle(x, y, x + w, y + h))
                        treeTrackers[currentTreeID] = tracker

                        # t = threading.Thread(target=_doRecognizePerson, args=(currentTreeID))
                        # t.start()
                        currentTreeID += 1

            if ann == "rect":
                # draw the rectangle for detected tree
                for tid in treeTrackers.keys():
                    tracked_position = treeTrackers[tid].get_position()
                    t_x = int(tracked_position.left())
                    t_y = int(tracked_position.top())
                    t_w = int(tracked_position.width())
                    t_h = int(tracked_position.height())
                    cv2.rectangle(resultImage, (t_x, t_y), (t_x + t_w, t_y + t_h), rectangleColor, 2)
            elif ann == "line":
                # draw line for the detected tree
                r_pts, l_pts = [], []
                r_cen, l_cen = (0.0, 0.0), (0.0, 0.0)
                for tid in treeTrackers.keys():
                    tracked_position = treeTrackers[tid].get_position()
                    t_x = tracked_position.left()
                    t_y = tracked_position.top()
                    t_w = tracked_position.width()
                    t_h = tracked_position.height()
                    pt = (int(t_x + t_w / 2), int(t_y + t_h / 2))

                    if t_x > (5.0 * width / 10.0):
                        r_pts.append(pt)
                        # r_cen = (r_cen[0] + t_x, r_cen[1] + t_y + t_h)
                        r_cen = (r_cen[0] + pt[0], r_cen[1] + pt[1])
                        # cv2.circle(resultImage, pt, 2, rightPointColor, 2)
                    elif t_x < (5.0 * width / 10.0):
                        l_pts.append(pt)
                        # l_cen = (l_cen[0] + t_x + t_w, l_cen[1] + t_y + t_h)  # right-bottom corner
                        l_cen = (l_cen[0] + pt[0], l_cen[1] + pt[1])
                        # cv2.circle(resultImage, pt, 2, leftPointColor, 2)

                if len(r_pts) != 0:
                    r_cen = (r_cen[0] / len(r_pts), r_cen[1] / len(r_pts))
                if len(l_pts) != 0:
                    l_cen = (l_cen[0] / len(l_pts), l_cen[1] / len(l_pts))

                # condition for detected rects
                if len(r_pts) > 3:
                    # half-right part
                    r_a = 0.0
                    cnt = 0
                    for i in range(len(r_pts) - 1):
                        for j in range(i + 1, len(r_pts)):
                            if (r_pts[i][0] - r_pts[j][0]) == 0:
                                continue
                            cnt += 1
                            r_a += math.atan(float(r_pts[i][1] - r_pts[j][1]) / float(r_pts[i][0] - r_pts[j][0]))
                    r_a = math.tan(r_a / cnt)
                    r_b = r_cen[1] - r_a * r_cen[0]

                if len(l_pts) > 3:
                    # half-left part

                    l_a = 0.0
                    cnt = 0
                    for i in range(len(l_pts) - 1):
                        for j in range(i + 1, len(l_pts)):
                            if (l_pts[i][0] - l_pts[j][0]) == 0:
                                continue
                            cnt += 1
                            l_a += math.atan(float(l_pts[i][1] - l_pts[j][1]) / float(l_pts[i][0] - l_pts[j][0]))
                    l_a = math.tan(l_a / cnt)
                    l_b = l_cen[1] - l_a * l_cen[0]

                if len(r_pts) <= 3 or len(l_pts) <= 3:
                    if len(r_pts) == 0:
                        r_b = r_a = 0
                    if len(l_pts) == 0:
                        l_b = l_a = 0
                    if 0 < len(r_pts) <= 3 and 0 < len(l_pts) <= 3:
                        if len(l_pts) == 0:
                            l_a, l_b = old_l_a, old_l_b
                        if len(r_pts) == 0:
                            r_a, r_b = old_r_a, old_r_b
                    if 0 < len(r_pts) <= 3 < len(l_pts):
                        r_a = - l_a
                        r_b = r_cen[1] - r_a * r_cen[0]
                    if len(r_pts) > 3 >= len(l_pts) > 0:
                        l_a = - r_a
                        l_b = l_cen[1] - l_a * l_cen[0]

                # draw line with alpha and b (Y = alpha x X + b)
                # sys.stdout.write("{}, {}\n".format(alpha, b))
                x1 = 1.0 * width / 10.0
                y1 = l_a * x1 + l_b
                x2 = 4.0 * width / 10.0
                y2 = l_a * x2 + l_b
                cv2.line(resultImage, (int(x1), int(y1)), (int(x2), int(y2)), (70, 150, 255), 2)
                # cv2.line(resultImage, (int(x1), int(y1)), (int(x2), int(y2)), (70, 150, 0), 2)

                x1_ = 6.0 * width / 10.0
                y1_ = r_a * x1_ + r_b
                x2_ = 9.0 * width / 10.0
                y2_ = r_a * x2_ + r_b
                cv2.line(resultImage, (int(x1_), int(y1_)), (int(x2_), int(y2_)), (70, 150, 255), 2)

                old_l_a, old_l_b, old_r_a, old_r_b = l_a, l_b, r_a, r_b

            cv2.imshow("result-image", resultImage)
            if save:
                out.write(resultImage)
            cv2.waitKey(1)

    except KeyboardInterrupt as e:
        pass

    # Destroy any OpenCV windows and exit the application
    cv2.destroyAllWindows()
    exit(0)


if __name__ == '__main__':

    arg.input_path = './orchard/video/sample3TrimTrim.mp4'
    arg.save = False
    arg.annotation = "line"

    """
    :params
    """
    g_my_cascade = []
    g_upsampling = 1.05
    g_distance = 1
    skip = 10

    # Load the trained model
    model_path = './model/myhaar_20_40_20.xml'
    if not os.path.isfile(model_path):
        raise Exception("There is no such xml files", model_path)
    g_my_cascade.append(cv2.CascadeClassifier(model_path))

    # Running the detection progress
    video = arg.input_path
    ann = arg.annotation
    bSave = arg.save

    if not str.isdigit(video) and not os.path.isfile(video):
        raise Exception("There is no such video file", video)
    videoDetectTree(video, bSave, skip, ann)
