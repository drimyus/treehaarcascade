import cv2
import os
import sys
import numpy as np
import time


def removeSpaceName_dir(dir_path):

    for f in os.listdir(dir_path):
        fn, ext = os.path.splitext(f)

        if ext.lower() == '.bmp':

            newf = f.replace(' ', '')

            out_fn = os.path.join(dir_path, newf)

            im = cv2.imread(os.path.join(dir_path, f))

            os.remove(os.path.join(dir_path, f))
            cv2.imwrite(out_fn, im)

            print ('source : ', fn, 'target : ', out_fn)


def resize_dir(dir_path):

    ratio = 1.5

    for f in os.listdir(dir_path):
        fn, ext = os.path.splitext(f)

        if ext.lower() == '.bmp':

            out_fn = os.path.join(dir_path, f)

            im = cv2.imread(os.path.join(dir_path, f))

            im = cv2.resize(im, (int(im.shape[1] / ratio), int(im.shape[0] / ratio)))

            print('source : ', fn, 'target : ', out_fn)
            cv2.imwrite(out_fn, im)


def jpg2bmp_dir(dir_path):

    for f in os.listdir(dir_path):
        fn, ext = os.path.splitext(f)

        if ext.lower() == '.jpg':

            out_fn = os.path.join(dir_path, fn + '.bmp')
            im = cv2.imread(os.path.join(dir_path, f))
            print ('source : ', fn, 'target : ', out_fn)

            cv2.imwrite(out_fn, im)


def _getNegativeColorFeature(neg_dir):
        
    # in case negative
    hist_steps = 32

    color = np.zeros(3)
    counts = 0
    for f in os.listdir(neg_dir):
        fn, ext = os.path.splitext(f)

        if ext.lower() == '.bmp':
            counts += 1
            im = cv2.imread(os.path.join(neg_dir, f))
            hist_blue = cv2.calcHist([im], [0], None, [hist_steps], [0, 256])
            hist_green = cv2.calcHist([im], [1], None, [hist_steps], [0, 256])
            hist_red = cv2.calcHist([im], [2], None, [hist_steps], [0, 256])

            blue = np.argmax(hist_blue) * 255 / hist_steps + 0.5 * 255 / hist_steps
            green = np.argmax(hist_green) * 255 / hist_steps + 0.5 * 255 / hist_steps
            red = np.argmax(hist_red) * 255 / hist_steps + 0.5 * 255 / hist_steps

            color[0] += blue
            color[1] += green
            color[2] += red

        ret_color = (color / counts).astype(dtype='int')

        if counts % 50 == 0 and counts != 0:
            sys.stdout.write('\r' + f + ':' + str(ret_color))
            sys.stdout.flush()
            time.sleep(0.1)

    return ret_color
    

def _getPositiveColorFeature(pos_dir):

    # in case positive

    # read the info file
    info_path = os.path.join(pos_dir, 'info.txt')
    text_file = open(info_path, "r")
    lines = text_file.readlines()

    hist_steps = 32
    color = np.zeros(3)
    counts = 0

    # scan image files with its name
    for line in lines:
        counts += 1
        items = line.split()

        pos = 0
        fn = items[pos]
        im = cv2.imread(os.path.join(pos_dir, fn))

        pos += 1
        lenRects = int(items[pos])

        pos += 1

        if pos + (4 * lenRects) > len(items):
            continue
        for i in range(lenRects):
            x = int(items[pos + (4 * i) + 0])
            y = int(items[pos + (4 * i) + 1])
            w = int(items[pos + (4 * i) + 2])
            h = int(items[pos + (4 * i) + 3])

            sub = im[y:y+h, x:x+w]

            hist_blue = cv2.calcHist([sub], [0], None, [hist_steps], [0, 256])
            hist_green = cv2.calcHist([sub], [1], None, [hist_steps], [0, 256])
            hist_red = cv2.calcHist([sub], [2], None, [hist_steps], [0, 256])

            blue = np.argmax(hist_blue) * 255 / hist_steps + 0.5 * 255 / hist_steps
            green = np.argmax(hist_green) * 255 / hist_steps + 0.5 * 255 / hist_steps
            red = np.argmax(hist_red) * 255 / hist_steps + 0.5 * 255 / hist_steps

            color[0] += blue/lenRects
            color[1] += green/lenRects
            color[2] += red/lenRects

        ret_color = (color / counts).astype(dtype='int')

        if counts % 50 == 0 and counts != 0:
            sys.stdout.write('\r' + fn + ':' + str(ret_color))
            sys.stdout.flush()
            time.sleep(0.1)

    return ret_color


def getColorFeature():

    """ Scan the Negative images folder and get out the color feature of negative regions
                Histogram analysis method was used. """
    negative_dir = './training/haar_utilites/negative'
    sys.stdout.write('Scanning Negatives... \n')
    sys.stdout.write('Negative Folder Path : ' + negative_dir + '\n')
    neg_color = _getNegativeColorFeature(negative_dir)
    print('\n Negative Color : ' + str(neg_color))

    """ Scan the Positive images folder and get out the color feature of Positive regions 
                    Histogram analysis method was used. """
    positive_dir = './training/haar_utilites/positive'
    sys.stdout.write('Scanning Positives... \n')
    sys.stdout.write('Positive Folder Path : ' + positive_dir + '\n')
    pos_color = _getPositiveColorFeature(positive_dir)
    print('\n Positive Color : ' + str(pos_color))


if __name__ == '__main__':

    # folder_path = './training/haar_utilites/positive/rawdata'

    # jpg2bmp_dir(folder_path)
    # removeSpaceName_dir(folder_path)

    getColorFeature()

